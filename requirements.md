R > 4.0.0

with libraries:
  - shiny
  - DT
  - dplyr
  - stringr
  - XML
  - rentrez
  - shinycssloaders
  - shinybusy
  - tm
  - SnowballC
  - wordcloud
  - RColorBrewer
  - ggplot2
  - gridExtra
  - ggridges
  - shinythemes

One line install :

```bash
Rscript requirements.R
```

Note that will also install R libraries to scan preprints (`rvest`, `aRxiv`, `httr`, `curl`, `jsonlite`).

You will also need :

  * openssl devel file (`libssl-dev` (debian/ubuntu), `openssl-devel` (fedora/redhat), `openssl@1.1` (Mac OSX)),
  * `libcurl` development library (`libcurl4-openssl-dev` (debian/ubuntu), `libcurl-devel` (fedora/redhat)),
  * `libicu` development library (`libicu-dev` (debian/ubuntu), `libicu-devel` (fedora/redhat)),

