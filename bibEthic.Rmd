---
title: "DAFNEE, a Database of Academia Friendly scientific jourNals in Ecology and Evolution."

output:
  html_document:
    toc: true
    #toc_float: true
    theme: journal
    highlight: tango
---

<style type="text/css">
.main-container {
  max-width: 2000px;
  margin-left: auto;
  margin-right: auto;
}
</style>
***   

###  **Motivations**

The scientific publishing market is widely recognized to be dysfunctional [1]. The average price of publishing is an order of magnitude higher than the real cost [2,3]. This anomaly essentially results from the "publish or perish" situation in which individual scientists are placed, and the power of large publishing groups with high profit margins [4]. 

Not all scientific journals, however, are economically equivalent. Some are run by non-profit organizations. Some are associated to learned societies, so that publication fees are partly reinvested in academia. Some are supported at moderate cost by scholarly institutions. Scientists are not always well informed about the complexity of journals' business models, and rarely include this criterion when deciding to interact with a journal as an author or reviewer. Yet, to support academia-friendly journals is a way to contribute to a fairer scientific publishing system.



###  **Database content**

The DAFNEE database offers a list of ~250 non-profit, learned society, museum or university-associated journals relevant to the field of ecology and evolutionary biology. The database includes generalist journals (e.g. eLife, PLoS Biol, PNAS, Science), flagship society journals (e.g. Am Nat, Ecology, JEB, Proc B, MBE, Syst Biol), Open Science initiatives (e.g. Peer Communiy In, MorphoMuseuM), and many high-quality journals in palaeo-archaeobiology, systematics, genetics, theoretical biology, organismal biology, environmental and health sciences. Journals can be queried by topic, business model, academic partnership, publication fees, and impact factor. In addition, an interactive tool gives every scientist the opportunity to evaluate and compare his/her own percentage of academia-friendly journal usage.

###  **Database curation**

The content of the database is updated every 6 months via the inspection of journal websites by a group of curators. New journals are added whenever they newly publish an article from Institut des Sciences de l'Evolution de Montpellier, or if identified by a curator. 
   
List of 'Ethical' journals where ISEM researchers published there papers. Last updated : `r format(Sys.time(), "%a %d %b %Y")`   

***  
 
```{r display_data, echo=FALSE, warning=FALSE}
library(DT)

ethicals <- read.table("./ethical_journals.csv", header=T, sep="\t", quote = '""')
ethicals = ethicals[, - which(colnames(ethicals) == "curator")] #d'ont show curators
ethicals = ethicals[, - which(colnames(ethicals) == "curator_notes")] #d'ont show curators_notes

#ethicals$curator = as.factor(ethicals$curator)

#Change those fields to factors facilitate filtering
ethicals$field = as.factor(ethicals$field)
ethicals$business_model = as.factor(ethicals$business_model)
ethicals$publisher = as.factor(ethicals$publisher)

ethicals$institution_type = as.factor(ethicals$institution_type)
#this allows to have a background proportional to the IF value
ethicals$IF = as.numeric(ethicals$IF)
#Render links clickable
ethicals$website[is.na(ethicals$website)] <- ""
ethicals$website <- paste0("<a href='",ethicals$website,"' target='_blank'>",ethicals$website,"</a>")

DATAB = datatable(ethicals, 
      
      options = list( pageLength = 25, 
      dom = "Bfrtip",
      buttons = list("copy", "csv",'excel', 'pdf'),
      initComplete = JS(
      "function(settings, json) {",
      "$(this.api().table().header()).css({'background-color': '#517fb9', 'color': '#fff'});",
      "}"
    )
     ), escape=FALSE, style = 'bootstrap',filter = 'top', editable = F,  selection = list(mode='single', target = 'row'),extensions = c( "Buttons") )

DATAB = DATAB %>%
        formatStyle("APC_euros",
                    background = styleColorBar(range(ethicals$APC_euros, na.rm=T), "lightblue"),
                    backgroundSize = '98% 88%',
                    backgroundRepeat = 'no-repeat',
                    backgroundPosition = 'center') 
DATAB = DATAB   %>%      
        formatStyle("IF",
                    background = styleColorBar(range(ethicals$IF, na.rm=T), "lightblue"),
                    backgroundSize = '98% 88%',
                    backgroundRepeat = 'no-repeat',
                    backgroundPosition = 'center')
DATAB                    
  
 
```

###  **The DAFNEE team**

**Curators**: Pierre-Olivier Antoine, Christophe Boëte, Frédéric Delsuc, Nicolas Galtier, Elise Huchard, Céline Scornavacca, Carole Smadja.

**Support**: Khalid Belkhir, Christine Bibal, Laure Paradis.

###  **Contact** 

nicolas.galtier@umontpellier.fr


###  **References**

[1] Rose-Wiles L M. 2011. The High Cost of Science Journals: A Case Study and Discussion.  Journal of Electronic Resources Librarianship 23:219-241.

[2] Alizon S. Inexpensive Research in the Golden Open-Access Era. Trends in Ecology and Evolution. 33:301-303.

[3] Grossmann A, Brembs B. 2021. Assessing the size of the affordability problem in scholarly publishing. Peer J Preprints https://peerj.com/preprints/27809/

[4] Walter P, Mullins D. 2019. From symbiont to parasite: the evolution of for-profit science publishing. Mol Biol Cell 30:2537-2542.