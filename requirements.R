#!/usr/bin/env R
install.packages(c("shiny", "DT", "dplyr", "stringr", "XML", "rentrez", "shinycssloaders", "shinybusy", "tm", "SnowballC", "wordcloud", "RColorBrewer", "ggplot2", "gridExtra", "ggridges", "shinythemes"), repos="https://cloud.r-project.org/")
# for preprints scans
install.packages(c("curl", "jsonlite", "rvest", "httr", "aRxiv"), repos="https://cloud.r-project.org/")
