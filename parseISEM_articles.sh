#awk 'BEGIN{FS="\t"} {if($2=="Article"){print} }' BibEthique/bd_biblio_1220.csv > articles_ISEM.csv


echo -e "lastname\tfirstname\tid\ttitle\tyear\tauthors\tjournal\tdoi\tabstract" > liste_Papers_personnel_publiant.tsv
while IFS=$'\t' read -r nom prenom autre; 
do     
    #>&2 echo $nom $prenom 
    #we can use the first Initiales to solve homonymous 

    #par ex Bonhomme V ou Antoine PO
    initiales=`awk -v prenom=$prenom 'BEGIN{nb =split(prenom,a,"-"); print substr(a[1],1,1) substr(a[2],1,1)}'`
    #par ex la ref 949 P.-O. Antoine
    dottedinitiales=`awk -v prenom=$prenom 'BEGIN{nb =split(prenom,a,"-"); if (nb == 1) {print substr(a[1],1,1)"\\\."} else {print substr(a[1],1,1)"\\\.-"substr(a[2],1,1)"\\\."} }'`

    # here we can have refs like :
    # Bonhomme, F
    grep  -i "${nom}, $initiales" articles_ISEM.csv | cut -d$'\t' -f1,3,4,6,7,10 | awk -v nom="${nom}" -v prenom="${prenom}" 'BEGIN{FS="\t"; OFS="\t"}{sub(/ [0-9]+.*/ , "" ,$5); if ($6=="") $10="?" ;print nom, prenom, $1,$4, $2,$3,$5,$6,"?" }' 
    # F. Bonhomme, ou P.O. Antoine,
    grep  -i "$dottedinitiales ${nom}, " articles_ISEM.csv | cut -d$'\t' -f1,3,4,6,7,10 | awk -v nom="${nom}" -v prenom="${prenom}" 'BEGIN{FS="\t"; OFS="\t"}{sub(/ [0-9]+.*/ , "" ,$5); if ($6=="") $10="?" ;print nom, prenom, $1,$4, $2,$3,$5,$6,"?" }' 
    # F. Bonhomme and
    grep  -i "$dottedinitiales ${nom} and " articles_ISEM.csv | cut -d$'\t' -f1,3,4,6,7,10 | awk -v nom="${nom}" -v prenom="${prenom}" 'BEGIN{FS="\t"; OFS="\t"}{sub(/ [0-9]+.*/ , "" ,$5); if ($6=="") $10="?" ;print nom, prenom, $1,$4, $2,$3,$5,$6,"?" }' 

done <  liste_personnel_ISEM_260321.csv >> liste_Papers_personnel_publiant.tsv

